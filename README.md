# Samambaia-Influence-Tests
 This project aims to apply and evaluate preliminar sensibility tests as an update of a previous Agent Based Modeling and Simulation
 developed by Pedro Porto and Guido Oliveira in 2019, which aims to analyze water usage for agriculture at the Samambaia river basin in
 Cristalina municipality, State of Goiás, Brazil. GAMA platform is used with GAML language.

 Model updates were developed in colaboration with professors Conceição Alves and Célia Ghedini, from the University of Brasília, Brazil.
